class Boton{
  PVector pos = new PVector(0,0);
  float width = 0;
  float height = 0;
  color colour;
  String text;
  Boolean pressed = false;
  boolean clicked = false;
  
  Boton(int x, int y, int w, int h, String t, int r, int g, int b){
    pos.x = x;
    pos.y = y;
    width = w;
    height = h;
    colour = color(r,b,g);
    text = t;
  }
  
  void update() {
    if (mousePressed && !pressed) {
      pressed = true;
      if (mouseX >= pos.x && mouseX <= pos.x + width && mouseY >= pos.y && mouseY <= pos.y + height) {
        clicked = true;
      }
    } else if (!mousePressed) {
      pressed = false;
    }
  }
  
  void render(){
    fill(colour);
    rect(pos.x, pos.y, width, height, 28);
    fill(0);
    textAlign(CENTER, CENTER);
    text(text,pos.x+(width/2), pos.y+(height/2));
  }
  
  boolean isClicked() {
    return clicked;
  }
  void resetClick() {
    clicked = false;
  }
  void setText(String newText) {
    text = newText;
  }
}
